#!/usr/bin/env python3


class Nodo(object):
    def __init__(self, indice= None,dato=None, prox = None):
        self.dato = dato
        self.indice = indice
        self.prox = prox
        
    
    def __str__(self):
        return str(self.dato)

def VerLista(nodo):
    """ Recorre todos los nodos a traves de sus enlaces,
        mostrando sus contenidos. """
    while nodo:
        # muestra el dato
        print (nodo)
        # ahora nodo apunta a nodo.prox
        nodo = nodo.prox



if __name__ == '__main__':
    v3 = Nodo("cambur")
    v2 = Nodo("fresa",v3)
    v1 = Nodo("manzana",v2)
    print (v1.dato)
    print(v2.dato)
    print(v3.dato)
    lista = v1
    VerLista(lista)
    print("*"*10)
    lista=lista.prox
    VerLista(lista)