#!/usr/bin/env python3

from nodos import Nodo

    

class ListaEnlazada(object):
    " Modela una lista enlazada, compuesta de Nodos. "
 
    def __init__(self):
        """ Crea una lista enlazada vacia. """
        # prim: apuntara al primer nodo -None con la lista vacia
        self.prim = None
        # len: longitud de la lista - 0 con la lista vacia
        self.len = 0

    #def __str__(self):
    #    return str(self)

    def verLista(self):
        while self.nuevo:
            # muestra el dato
            print (self.nuevo)
            # ahora nodo apunta a nodo.prox
            self.nodo = self.nuevo.prox

    def __len__(self):
        return self.len


    def remove(self, x):
        """ Borra la primera aparicion del valor x en la lista.
        Si x no esta en la lista, levanta ValueError """
        if self.len == 0:
            # Si la lista esta vacia, no hay nada que borrar.
            raise ValueError("Lista vacia")
 
        # Caso particular, x esta en el primer nodo
        elif self.prim.dato == x:
            # Se descarta la cabecera de la lista
            self.prim = self.prim.prox
 
        # En cualquier otro caso, hay que buscar a x
        else:
            # Obtiene el nodo anterior al que contiene a x (n_ant)
            n_ant = self.prim
            n_act = n_ant.prox
            while n_act != None and n_act.dato != x:
                n_ant = n_act
                n_act = n_ant.prox
 
            # Si no se encontro a x en la lista, levanta la excepcion
            if n_act == None:
                raise ValueError("El valor no ester en la lista.")
 
            # Si encontro a x, debe pasar de n_ant -> n_x -> n_x.prox
            # a n_ant -> n_x.prox
            else:
                n_ant.prox = n_act.prox
 
        # Si no levanto excepcion, hay que restar 1 del largo
        self.len -= 1
 
        if (i > self.len) or (i < 0):
            # error
            raise IndexError("Posicion invalida")

    def insert(self, i, x):
        """ Inserta el elemento x en la posicion i.
        Si la posicion es invalida, levanta IndexError """
        if (i > self.len) or (i < 0):
            # error
            raise IndexError("Posicion invalida")

        # Crea nuevo nodo, con x como dato:
        self.nuevo = Nodo(x)

        # Insertar al principio (caso particular)
        if i == 0:
            # el siguiente del nuevo pasa a ser el que era primero
            self.nuevo.prox = self.prim
            # el nuevo pasa a ser el primero de la lista
            self.prim = self.nuevo
        # Insertar en cualquier lugar > 0
        else:
            # Recorre la lista hasta llegar a la posicion deseada
            n_ant = self.prim
            for pos in xrange(1,i):
                n_ant = n_ant.prox
 
            # Intercala nuevo y obtiene n_ant -> nuevo -> n_ant.prox
            self.nuevo.prox = n_ant.prox
            n_ant.prox = self.nuevo

        # En cualquier caso, incrementar en 1 la longitud
        self.len += 1

    def append(self,x):
        """ Inserta el elemento x en la posicion i.
        Si la posicion es invalida, levanta IndexError """
        

        # Crea nuevo nodo, con x como dato:
        self.nuevo = Nodo(x)

        # Insertar al principio (caso particular)
        if self.len == 0:
            # el siguiente del nuevo pasa a ser el que era primero
            self.nuevo.prox = self.prim
            # el nuevo pasa a ser el primero de la lista
            self.prim = self.nuevo
        # Insertar en cualquier lugar > 0
        else:
            # Recorre la lista hasta llegar a la posicion deseada
            n_ant = self.prim
            for pos in xrange(1,self.len):
                n_ant = n_ant.prox
 
            # Intercala nuevo y obtiene n_ant -> nuevo -> n_ant.prox
            self.nuevo.prox = n_ant.prox
            n_ant.prox = self.nuevo

        # En cualquier caso, incrementar en 1 la longitud
        self.len += 1



    def pop(self,i=None):
        """ Elimina el nodo de la posicion i, y devuelve el dato contenido.
        Si i esta fuera de rango, se levanta la excepcion IndexError.
        Si no se recibe la posicion, devuelve el ultimo elemento. """
 
        # Si no se recibio i, se devuelve el ultimo.
        if i is None:
            i = self.len - 1
 
        # Verificacion de los limites
        if not (0 <= i < self.len):
            raise IndexError("indice fuera de rango")
 
        # Caso particular, si es el primero,
        # hay que saltear la cabecera de la lista
        if i == 0:
            dato = self.prim.dato
            self.prim = self.prim.prox
 
        # Para todos los demas elementos, busca la posicion
        else:
            n_ant = self.prim
            n_act = n_ant.prox
            for pos in xrange(1, i):
                n_ant = n_act
                n_act = n_ant.prox
 
            # Guarda el dato y elimina el nodo a borrar
            dato = n_act.dato
            n_ant.prox = n_act.prox
 
        # hay que restar 1 de len
        self.len -= 1
        # y devolver el valor borrado
        return dato



if __name__ == '__main__':
    lista = ListaEnlazada()
    #print(lista)
    lista.append("manzana")
    lista.append("pera")
    lista.append("fresa")
    print(lista.pop())