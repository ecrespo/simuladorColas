#!/usr/bin/env python3

import numpy as np 
from math import log
import string 
MAX_NODOS = 20;
MAX_SUCESORES = 5;
MAX_REAL = 1.7e38;


class tipo(object):
	def __init__(self,llegadaI=None,llegadaE=None,salida=None,otro=None,ultimo=None):
		self.llegadaI = llegadaI
		self.llegadaE = llegadaE
		self.salida = salida
		self.otro = otro
		self.ultimo = ultimo


class Nodo(object):
    def __init__(self,llegaron,servidos,tLlegadas,tServicio,tTotalEspera,tEvenPrev,\
    	tVacio,utilPond,capacidad,utilizacion,cola,colaIni,colaMax,numSuc,suc=[],probSuc=[]):
        self.tipoN = tipo() #Falta agregar valores.
        self.llegaron = llegaron
        self.servidos = servidos
        self.tLlegadas = tLlegadas
        self.tServicio = tServicio
        self.tTotalEspera = tTotalEspera
        self.tEvenPrev = tEvenPrev
        self.tVacio = tVacio
        self.utilPond = utilPond
        self.capacidad = capacidad
        self.utilizacion = utilizacion
        self.cola = cola
        self.colaIni = colaIni
        self.colaMax = colaMax
        self.numSuc = numSuc
        self.suc = suc
        self.probSuc = probSuc
        


class evento(object):
	def __init__(self,tiempo=0.0,nodo=0,tipo=None,proximo=None):
		self.tiempo = tiempo
		self.nodo = nodo
		self.tipo = tipo
		self.proximo = proximo


class red(object):
	def __init__(self,numNodos,nodos=[]):
		self.numNodos = numNodos

	def aleaExp(self,media):
		np.random.seed(0)
		return -media*log(np.random.rand())


	def leerNomArchivo(self):
		print()
		self.nombreE = input('Nombre del archivo de entrada: ')
		if string.find(nombreE,".") == -1:
			self.nombreE = nombreE + ".def"
		self.aEntrada = self.nombreE
		self.nombreS = input('Nombre del archivo de salida (con -> pantalla:) ')
		if len(self.nombreS) == 0:
			self.nombreS = self.nombreE.split(".")[0] + '.out'
		self.aSalida = self.nombreS

	def leerRed(self):
		"""
		Lee la definicion de la red de un archivo con extension '.red' y la
		almacena en la estructura 'red'
		La primera linea es el tiempo de simulacion (Real) y el numero inicial de
		clientes en el sistema, que sera repartido uniformemente entre los nodos.
		Luego el archivo debe definir nodo por nodo de la siguiente forma:
		Tipo del nodo: 1 -> Llegada, otro entero -> Otro (Enteros)
		Si el nodo es de tipo 'Llegada':
		Tiempo entre llegadas, de servicio y capacidad (Real, Real, Entero)
		Si el nodo es es tipo 'Otro':
		Tiempo de servicio y capacidad (Real, Entero)
		Numero de sucesores (Entero - si no hay debe ser 0)
		Sucesores - si los hay (Enteros)
		Distribucion de probabilidades - si hay sucesores (Reales)"""
		


