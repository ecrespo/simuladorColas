program redes;
uses Sysutils;

const Max_nodos = 20;
      Max_sucesores = 5;
      Max_real = 1.7e38;


type tipo_ = (Llegada,Llegada_i,Llegada_e,Salida,Otro,Ultimo);


nodo = record
  tipo_n : tipo_;
  llegaron, servidos,t_llegadas, t_servicio, t_total_esp,
    t_even_prev, t_vacio, util_pond : real;
  capacidad, utilizacion, cola, cola_ini, cola_max, num_suc : longint;
  suc : array[1..Max_sucesores] of integer;
  prob_suc : array[1..Max_sucesores] of real;
end;

apt_evento = ^evento_;
evento_ = record
  tiempo : real;
  nodo : integer;
  tipo : tipo_;
  proximo : apt_evento;
end;

red_ = record
  num_nodos : integer;
  nodos : array[1..Max_nodos] of nodo_;
end;



var red : red_;
    TSIM : real;
    i, nodo_actual : integer;
    primero, primero_pv,
    evento, evento_a : apt_evento;
    nombre_e, nombre_s : string[12];
    tiempo_sim, X : real;
   a_entrada, a_salida : text;
   t_inicial, t_final, t_dif : comp;

function alea_exp(media : real) : real;
   (*
   Genera numeros aleatorios con distribucion exponencial de media 'media'
   *)
begin
  alea_exp := -media*ln(random);
end;


procedure lee_nom_arch;
var i : integer;
begin
  writeln;
  write('Nombre del archivo de entrada: ');readln(nombre_e);
  if Pos('.',nombre_e) = 0 then nombre_e := nombre_e+'.def';
  assign(a_entrada,nombre_e);
  reset(a_entrada);
  writeln;
  write('Nombre del archivo de salida (con -> pantalla:) ');readln(nombre_s);
  If nombre_s = '' then
    nombre_s := Copy(nombre_e,1,Pos('.',nombre_e)) + 'out';
  for i := 1 to length(nombre_s) do nombre_s[i] := upcase(nombre_s[i]);
  assign(a_salida,nombre_s);
  rewrite(a_salida);
end;


procedure lee_red;
(*
Lee la definicion de la red de un archivo con extension '.red' y la
almacena en la estructura 'red'
La primera linea es el tiempo de simulacion (Real) y el numero inicial de
clientes en el sistema, que sera repartido uniformemente entre los nodos.
Luego el archivo debe definir nodo por nodo de la siguiente forma:
Tipo del nodo: 1 -> Llegada, otro entero -> Otro (Enteros)
Si el nodo es de tipo 'Llegada':
Tiempo entre llegadas, de servicio y capacidad (Real, Real, Entero)
Si el nodo es es tipo 'Otro':
Tiempo de servicio y capacidad (Real, Entero)
Numero de sucesores (Entero - si no hay debe ser 0)
Sucesores - si los hay (Enteros)
Distribucion de probabilidades - si hay sucesores (Reales)
*)
var i, j , num_cli: integer;

begin
  readln(a_entrada,TSIM, num_cli);
  i := 0;
  with red do begin
    while not eof(a_entrada) do begin
    i := i + 1;
    with nodos[i] do begin
      readln(a_entrada,j);
      if j=1 then tipo_n := Llegada
      else tipo_n := Otro;
      if tipo_n = Llegada then read(a_entrada,t_llegadas);
      readln(a_entrada,t_servicio,capacidad);
      readln(a_entrada,num_suc);
      if num_suc > 0 then begin
        for j := 1 to num_suc do read(a_entrada,suc[j]);
        readln(a_entrada);
        for j := 1 to num_suc do read(a_entrada,prob_suc[j]);
        readln(a_entrada);
      end;
      { inicializacion del resto de los parametros }
      t_total_esp := 0; t_even_prev := 0;
      llegaron := 0;
      cola := 0;
      t_vacio := 0;
      cola_max := 0;
      servidos := 0;
      util_pond := 0;
      utilizacion := 0;
      cola_ini := 0;
    end;
  end;
  num_nodos := i;
end;
{ Reparte los clientes iniciales uniformemente entre los nodos }
i := 1;
for j:=1 to num_cli do begin
  red.nodos[i].cola := red.nodos[i].cola + 1;
  i := i + 1;
  if i > red.num_nodos then i := 1;
end;
close(a_entrada);
end;



procedure imprime_red;
(*
Imprime la definicion de la red almacenada en la estructura 'red'
*)
var i, j : integer;
begin
  writeln(a_salida);
  writeln(a_salida,'Archivo: ',nombre_e);
  writeln(a_salida);
  with red do
    for i := 1 to num_nodos do
      with nodos[i] do begin
      if tipo_n = Llegada then
        writeln(a_salida,'Nodo: ',i:2,' Tipo: Llegada')
      else writeln(a_salida,'Nodo: ',i:2,' Tipo: Otro');
      if tipo_n = Llegada then
        write(a_salida,'T_E_Llegadas: ',t_llegadas:5:2);
      writeln(a_salida,' T_Servicio: ',t_servicio:5:2,
'                             Capacidad: ',capacidad:3);
      write(a_salida,'Sucesores: ');
      for j := 1 to num_suc do
        write(a_salida,suc[j],' (',prob_suc[j]:3:2,')');
      writeln(a_salida); writeln(a_salida);
   end;
end;

procedure rec_LEP;
(*
Recorre e imprime los eventos en la LEP. Se uso para fines de depuracion
pero no es necesario para la simulacion.
*)
var evento : apt_evento;
begin
  evento := primero^.proximo;
  while evento <> nil do begin
    with evento^ do begin
      if tipo = Llegada then write('Llegada ')
      else write('Salida ');
      writeln('nodo: ',nodo,' tiempo: ',tiempo:9:2);
    end;
    evento := evento^.proximo;
  end;
end;

procedure inicia_LEP;
(*
Inicializa la Lista de Eventos Pendientes: crea 'primero', 'ultimo' y las
primeras llegadas.
*)
var n_nodo,i,iniciales : integer;
begin
  new(evento);
  with evento^ do begin
    tiempo := Max_Real;;
    nodo := 0;
    tipo := Ultimo;
    proximo := nil;
  end;
  new(primero);
  primero^.proximo := evento;
  for n_nodo := 1 to red.num_nodos do
    with red.nodos[n_nodo] do begin
      if tipo_n = Llegada then begin
        new(evento);
        with evento^ do begin
          tiempo := alea_exp(t_llegadas);
          nodo := n_nodo;
          tipo := Llegada_e;
        end;
        inserta(evento);
      end;
      if cola > 0 then begin
        cola_ini := cola;
        cola_max := cola;
        llegaron := cola;
        if cola > capacidad then iniciales := capacidad
        else iniciales := cola;
        for i:=1 to iniciales do begin
          cola := cola - 1;
          utilizacion := utilizacion + 1;
          new(evento);
          with evento^ do begin
            tiempo := alea_exp(t_servicio);
            nodo := n_nodo;
            tipo := Salida;
          end;
          inserta(evento);
        end;
      end;
    end;
    primero_pv := nil;
end;


function prox_evento : apt_evento;
(*
Retorna el proximo evento a procesar y actualiza el tiempo simulado.
*)
var evento : apt_evento;
begin
  evento := primero^.proximo;
  primero^.proximo := evento^.proximo;
  prox_evento := evento;
  tiempo_sim := evento^.tiempo;
end;

(* Procedimientos para obtener y retornar registros de eventos en el pull
de eventos vacios *)
procedure new_n(var evento : apt_evento);
begin
  if primero_pv <> nil then begin
    evento := primero_pv;
    primero_pv := evento^.proximo;
  end else new(evento);
end;


procedure dispose_n(var evento : apt_evento);
begin
  evento^.proximo := primero_pv;
  primero_pv := evento;
end;
begin
  randomize;
  lee_nom_arch;
  lee_red;
  imprime_red;
  t_inicial := TimeStampToMSecs(DateTimeToTimeStamp(Time));
  tiempo_sim := 0;
  inicia_LEP;
  evento := prox_evento;
  while tiempo_sim < TSIM do begin
    with evento^ do begin
      nodo_actual := nodo;
      with red.nodos[nodo_actual] do begin
        t_total_esp := t_total_esp + cola*(tiempo_sim - t_even_prev);
        util_pond := util_pond + utilizacion*(tiempo_sim - t_even_prev);
        if tipo <> Salida then begin
          llegaron := llegaron + 1;
          if utilizacion = capacidad then begin
            cola := cola + 1;
            if cola > cola_max then cola_max := cola;
          end else begin
            if utilizacion = 0 then
              t_vacio := t_vacio + (tiempo_sim - t_even_prev);
            new_n(evento_a);
            with evento_a^ do begin
              tiempo := tiempo_sim + alea_exp(t_servicio);
              nodo := nodo_actual;
              tipo := Salida;
            end;
            inserta(evento_a);
            utilizacion := utilizacion + 1;
          end;
          if tipo = Llegada_e then begin
            tiempo := tiempo_sim + alea_exp(t_llegadas);
            inserta(evento)
          end else dispose_n(evento);
        end else begin
          if num_suc > 0 then begin
            X := random;
            i := 1;
            while prob_suc[i] < X do i := i + 1;
            new_n(evento_a);
            with evento_a^ do begin
              tiempo := tiempo_sim;
              nodo := suc[i];
              tipo := Llegada_i;
            end;
            inserta(evento_a);
          end;
          servidos := servidos + 1;
          if cola > 0 then begin
            cola := cola - 1;
            tiempo := tiempo_sim + alea_exp(t_servicio);
            inserta(evento);
          end else begin
            utilizacion := utilizacion - 1;
            dispose_n(evento);
          end;
        end;
        t_even_prev := tiempo_sim;
      end;
    end;
    evento := prox_evento;
  end;
  t_final := TimeStampToMSecs(DateTimeToTimeStamp(Time));
  t_dif := t_final - t_inicial;
  writeln(a_salida);
  write(a_salida,'Tiempo de simulacion: ',TSIM:8:0,'Tiempo de corrida: ');
  writeln(a_salida,t_dif/1000:6:2,'s');
  writeln(a_salida);
  writeln(a_salida,
  'Nod Llega Servi EnSer LCol LColM ColI TProEsp LProCol T Vacio OcupPro');
  writeln(a_salida,
  '=== ====== ====== ===== ===== ===== ===== ======= ======= ========= =======');
  for i:=1 to red.num_nodos do
    with red.nodos[i] do begin
      if utilizacion = 0 then t_vacio := t_vacio + (TSIM - t_even_prev)
      else util_pond := util_pond + utilizacion*(TSIM - t_even_prev);
      t_total_esp := t_total_esp + cola*(TSIM - t_even_prev);
      writeln(a_salida,
            ' ',i:2,' ',llegaron:6:0,' ',servidos:6:0,' ',utilizacion:5,' ',
            cola:5,' ',cola_max:5,' ',cola_ini:5,' ',
            t_total_esp/llegaron:7:1,' ',t_total_esp/TSIM:7:1,' ',
            t_vacio:9:2,' ',util_pond/TSIM:7:2);
    end;
  if nombre_s = 'CON' then readln;
  close(a_salida);
end.
